# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20131018123600) do

  create_table "funcionarios", force: true do |t|
    t.string   "nome"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
  end

  add_index "funcionarios", ["email"], name: "index_funcionarios_on_email", unique: true, using: :btree
  add_index "funcionarios", ["reset_password_token"], name: "index_funcionarios_on_reset_password_token", unique: true, using: :btree

  create_table "lotes", force: true do |t|
    t.string   "codigo"
    t.integer  "operadora_id"
    t.integer  "funcionario_id"
    t.integer  "quantidade_cartoes"
    t.integer  "quantidade_kits"
    t.datetime "data_entrada"
    t.datetime "data_entrega"
    t.datetime "data_inicio"
    t.datetime "data_termino"
    t.boolean  "prioridade"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "encarregado_id"
  end

  create_table "operadoras", force: true do |t|
    t.string   "nome"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "produtos", force: true do |t|
    t.string   "codigo"
    t.string   "codigo_sericard"
    t.string   "orientador"
    t.date     "edicao"
    t.integer  "grade"
    t.integer  "estoque_atual"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "enderecamento"
  end

end
