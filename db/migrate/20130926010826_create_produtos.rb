class CreateProdutos < ActiveRecord::Migration
  def change
    create_table :produtos do |t|
      t.string :codigo
      t.string :codigo_sericard
      t.string :orientador
      t.date :edicao
      t.integer :grade
      t.integer :estoque_atual
      t.integer :reposicao

      t.timestamps
    end
  end
end
