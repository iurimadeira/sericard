class CreateRequisicoes < ActiveRecord::Migration
  def change
    create_table :requisicoes do |t|
      t.integer :produto_id
      t.integer :quantidade

      t.timestamps
    end
  end
end
