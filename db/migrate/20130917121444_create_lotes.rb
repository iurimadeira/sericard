class CreateLotes < ActiveRecord::Migration
  def change
    create_table :lotes do |t|
      t.string :codigo
      t.integer :operadora
      t.integer :funcionario
      t.integer :quantidade_cartoes
      t.integer :quantidade_kits
      t.date :data_entrada
      t.date :data_entrega
      t.date :data_inicio
      t.date :data_termino
      t.boolean :prioridade

      t.timestamps
    end
  end
end
