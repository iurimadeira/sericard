class ChangeDatesToDateTime < ActiveRecord::Migration
  def self.up
    change_column :lotes, :data_entrada, :datetime
    change_column :lotes, :data_entrega, :datetime
    change_column :lotes, :data_inicio, :datetime
    change_column :lotes, :data_termino, :datetime
  end

  def self.down
    change_column :lotes, :data_entrada, :date
    change_column :lotes, :data_entrega, :date
    change_column :lotes, :data_inicio, :date
    change_column :lotes, :data_termino, :date
  end  
end
