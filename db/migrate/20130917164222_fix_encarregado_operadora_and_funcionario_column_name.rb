class FixEncarregadoOperadoraAndFuncionarioColumnName < ActiveRecord::Migration
  def change
  	rename_column :lotes, :encarregado, :encarregado_id
  	rename_column :lotes, :operadora, :operadora_id
  	rename_column :lotes, :funcionario, :funcionario_id
  end
end
