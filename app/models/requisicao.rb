class Requisicao < ActiveRecord::Base
  belongs_to :lote
  paginates_per 30
  
  def dar_baixa
    self.produto.estoque_atual -= self.quantidade
  end  
  
end
