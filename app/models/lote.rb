class Lote < ActiveRecord::Base
	
  has_many :requisicoes
  
  belongs_to :funcionario
	belongs_to :operadora
	belongs_to :encarregado, :class_name => 'Funcionario'

	paginates_per 30

	validates_presence_of :operadora_id, :encarregado_id, :codigo, :quantidade_cartoes, :quantidade_kits, :data_entrada, :data_entrega

	scope :default_order, -> {joins(:operadora).where("operadoras.id = lotes.operadora_id").order("data_entrada DESC, operadoras.nome ASC")}
	scope :default_estados, -> {estados([:atrasado, :nao_iniciado, :em_producao])}
	scope :estados, lambda {|estados| where(id: all.select{ |lote| estados.include? lote.estado}.map(&:id))}	
	scope :find_by_codigo, lambda {|codigo| where("codigo like ?", "%#{codigo}%")}	

	def estado

		#Estados em ordem de prioridade
		
		# Atrasado, quando já passou da data de entrega
		if self.data_entrega && self.data_entrega < Date.today && self.data_termino.blank?
			return :atrasado
		
		# Não iniciado, quando não tem data de início	
		elsif self.data_inicio.blank?
			return :nao_iniciado
		
		# Em produção, se tiver data de inicio, mas não de termino
		elsif self.data_inicio.blank? == false && self.data_termino.blank?
			return :em_producao
		
		# Finalizado, quando possui data de término
		elsif self.data_termino.blank? == false
			return :finalizado
		end	

	end

	def estado?(estado)
		self.estado == estado
	end

	def estado_to_bootstrap
		case self.estado
			when :nao_iniciado then "warning"
			when :finalizado then "success"
			when :atrasado then "error"
			when :em_producao then "info"
		end
	end	

end
