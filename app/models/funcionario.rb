class Funcionario < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  	devise :database_authenticatable, :trackable

  	validates_presence_of :nome, :password, :password_confirmation
	
	has_many :lotes_cadastrados, :class_name => "Lote", :foreign_key => "funcionario_id"
	has_many :lotes_produzidos, :class_name => "Lote", :foreign_key => "encarregado_id"
end
