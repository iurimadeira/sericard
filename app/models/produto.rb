class Produto < ActiveRecord::Base

	validates_presence_of :codigo, :codigo_sericard, :orientador, :edicao, :grade, :estoque_atual, :enderecamento

	paginates_per 30

	scope :default_order, -> {order("codigo_sericard ASC")}
	scope :by_codigo, lambda {|codigo| where("codigo like ?", "#{codigo}")}
	scope :search, lambda {|codigo| where("codigo like ?", "%#{codigo}%")}

  # Pega reposicao
  def reposicao
    reposicao = self.estoque_atual - self.grade
    reposicao
	end	
  
  # Checa se o estoque esta abaixo do nivel de reposicao
	def reposicao?
    if reposicao > 0
      return true
    end
      
    false      
	end

	# Checa se existe produto no estoque
	def estoque?
		return true unless self.estoque_atual < 1
		false
	end	

	# Verifica a cor da row de acordo com o estado de estoque
	def row_class
    
		if reposicao?
      return "success"
		else
      return "error"
		end		

	end



end
