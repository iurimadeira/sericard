class RequisicoesController < ApplicationController
  before_action :set_requisicao

  # GET /requisicoes
  # GET /requisicoes.json
  def index_entrada
    @lotes = Lote.all
  end
  
  def entrada
    
  end
  
  def index_baixa
    @requisicoes = Requisicao.all
  end

  def baixa
    @requisicao = Requisicao.find(params[:id])
    @requisicao.dar_baixa    
  end

  # GET /requisicoes/1
  # GET /requisicoes/1.json
  def show
  end

  # GET /requisicoes/new
  def new
    @requisicao = Requisicao.new
  end

  # GET /requisicoes/1/edit
  def edit
  end

  # POST /requisicoes
  # POST /requisicoes.json
  def create
    @requisicao = Requisicao.new(requisicao_params)

    respond_to do |format|
      if @requisicao.save
        format.html { redirect_to @requisicao, notice: 'Requisicao was successfully created.' }
        format.json { render action: 'show', status: :created, location: @requisicao }
      else
        format.html { render action: 'new' }
        format.json { render json: @requisicao.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /requisicoes/1
  # PATCH/PUT /requisicoes/1.json
  def update
    respond_to do |format|
      if @requisicao.update(requisicao_params)
        format.html { redirect_to @requisicao, notice: 'Requisicao was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @requisicao.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /requisicoes/1
  # DELETE /requisicoes/1.json
  def destroy
    @requisicao.destroy
    respond_to do |format|
      format.html { redirect_to requisicoes_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_requisicao
      @requisicao = Requisicao.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def requisicao_params
      params.require(:requisicao).permit(:produto_id, :quantidade)
    end
end
