class ProdutosController < ApplicationController
  before_action :set_produto, only: [:edit, :update, :destroy]

  # GET /produtos
  # GET /produtos.json
  def index
    @produtos = Produto.all.default_order.page params[:page]
  end

  # GET /produtos/1
  # GET /produtos/1.json
  def show
  end

  # GET /produtos/new
  def new
    @produto = Produto.new
  end

  # GET /produtos/1/edit
  def edit
  end

  # POST /produtos
  # POST /produtos.json
  def create
    @produto = Produto.new(produto_params)

    respond_to do |format|
      if @produto.save
        format.html { redirect_to produtos_path }
        format.json { render action: 'show', status: :created, location: @produto }
      else
        format.html { render action: 'new' }
        format.json { render json: @produto.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /produtos/1
  # PATCH/PUT /produtos/1.json
  def update
    respond_to do |format|
      if @produto.update(produto_params)
        format.html { redirect_to produtos_path }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @produto.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /produtos/1
  # DELETE /produtos/1.json
  def destroy
    @produto.destroy
    respond_to do |format|
      format.html { redirect_to produtos_url }
      format.json { head :no_content }
    end
  end

  def entrada
    @produto = Produto.by_codigo(params[:codigo]).first
    @produto.estoque_atual += params[:quantidade].to_i
    @produto.save

    respond_to do |format|
      format.html { redirect_to produtos_path }
    end
  end
  
  def saida
    @produto = Produto.by_codigo(params[:codigo]).first
    if @produto.estoque? 
      @produto.estoque_atual -= params[:quantidade].to_i
      @produto.save
    end

    respond_to do |format|
      format.html { redirect_to produtos_path }
    end
  end

  def search
    @codigo = params[:codigo]
    @produtos = Produto.search(@codigo).page params[:page]
  end  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_produto
      @produto = Produto.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def produto_params
      params.require(:produto).permit(:codigo, :codigo_sericard, :orientador, :edicao, :grade, :estoque_atual, :enderecamento)
    end
end
