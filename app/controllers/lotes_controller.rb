class LotesController < ApplicationController
  before_action :set_lote, only: [:show, :edit, :update, :destroy]

  # GET /lotes
  # GET /lotes.json
  def index
    @lotes = Lote.default_estados.default_order.page params[:page]
  end

  # GET /lotes/1
  # GET /lotes/1.json
  def show
  end

  # GET /lotes/new
  def new
    @lote = Lote.new
  end

  # GET /lotes/1/edit
  def edit
  end

  # POST /lotes
  # POST /lotes.json
  def create
    @lote = Lote.new(lote_params)
    @lote.funcionario_id = current_funcionario.id
    

    respond_to do |format|
      if @lote.save
        format.html { redirect_to lotes_path, notice: 'Lote was successfully created.' }
        format.json { render action: 'show', status: :created, location: @lote }
      else
        format.html { render action: 'new' }
        format.json { render json: @lote.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /lotes/1
  # PATCH/PUT /lotes/1.json
  def update
    respond_to do |format|
      if @lote.update(lote_params)
        format.html { redirect_to lotes_path, notice: 'Lote was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @lote.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lotes/1
  # DELETE /lotes/1.json
  def destroy
    @lote.destroy
    respond_to do |format|
      format.html { redirect_to lotes_url }
      format.json { head :no_content }
    end
  end

  def search
    @codigo = params[:codigo]
    @lotes = Lote.find_by_codigo(@codigo).default_order.page params[:page]
  end  

  def relatorio
    @lote = Lote.find(params[:id])    

    respond_to do |format|
      format.pdf {
        html = render_to_string(:layout => false , :action => "relatorio.html.erb")
        kit = PDFKit.new(html, :orientation => 'Landscape')
        kit.stylesheets << "#{Rails.root}/app/assets/stylesheets/scaffolds.css.scss"
        send_data(kit.to_pdf, :filename => "#{@lote.codigo}.pdf",
          :type => 'application/pdf', :disposition => 'inline')        
        return
      }
    end
  end  

  def finalizados
    @lotes = Lote.estados([:finalizado]).default_order.page params[:page]
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_lote
      @lote = Lote.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def lote_params
      params.require(:lote).permit(:codigo, :encarregado_id, :operadora_id, :quantidade_cartoes, :quantidade_kits, :data_entrada, :data_entrega, :data_inicio, :data_termino, :prioridade)
    end
end
