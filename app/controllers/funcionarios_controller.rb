class FuncionariosController < ApplicationController
  before_action :set_funcionario, only: [:show, :edit, :update, :destroy]
  before_action :autoriza, except: [:show]

  def autoriza
    autorizados = [1, 2]
    redirect_to lotes_path unless autorizados.include? current_funcionario.id
  end

  # GET /funcionarios
  # GET /funcionarios.json
  def index
    @funcionarios = Funcionario.all
  end

  # GET /funcionarios/1
  # GET /funcionarios/1.json
  def show
  end

  # GET /funcionarios/new
  def new
    @funcionario = Funcionario.new    
  end

  # GET /funcionarios/1/edit
  def edit
  end

  # POST /funcionarios
  # POST /funcionarios.json
  def create        

      @funcionario = Funcionario.new(funcionario_params)
      @funcionario.email = "#{Funcionario.all.size}#{Time.now.to_f}@sericard.com.br"
      
      respond_to do |format|
        if @funcionario.save
          format.html { redirect_to funcionarios_path, notice: 'Funcionario was successfully created.' }
          format.json { render action: 'show', status: :created, location: @funcionario }
        else
          format.html { render action: 'new' }
          format.json { render json: @funcionario.errors, status: :unprocessable_entity }
        end
      end      
    
  end

  # PATCH/PUT /funcionarios/1
  # PATCH/PUT /funcionarios/1.json
  def update   

      @funcionario.email = "#{@funcionario.id}@sericard.com.br"

      respond_to do |format|
        if @funcionario.update(funcionario_params)
          format.html { redirect_to @funcionario, notice: 'Funcionario was successfully updated.' }
          format.json { head :no_content }
        else
          format.html { render action: 'edit' }
          format.json { render json: @funcionario.errors, status: :unprocessable_entity }
        end
      end
    
  end

  # DELETE /funcionarios/1
  # DELETE /funcionarios/1.json
  def destroy    

      @funcionario.destroy
      respond_to do |format|
        format.html { redirect_to funcionarios_url }
        format.json { head :no_content }
      end
    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_funcionario
      @funcionario = Funcionario.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def funcionario_params
      params.require(:funcionario).permit(:nome, :password, :password_confirmation)
    end
end
