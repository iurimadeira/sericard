json.array!(@produtos) do |produto|
  json.extract! produto, :codigo, :codigo_sericard, :orientador, :edicao, :grade, :estoque_atual, :reposicao
  json.url produto_url(produto, format: :json)
end
