json.array!(@lotes) do |lote|
  json.extract! lote, :codigo, :operadora, :funcionario, :quantidade_cartoes, :quantidade_kits, :data_entrada, :data_entrega, :data_inicio, :data_termino, :prioridade
  json.url lote_url(lote, format: :json)
end
