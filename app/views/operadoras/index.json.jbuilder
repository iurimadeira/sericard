json.array!(@operadoras) do |operadora|
  json.extract! operadora, :nome
  json.url operadora_url(operadora, format: :json)
end
