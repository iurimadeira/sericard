json.array!(@funcionarios) do |funcionario|
  json.extract! funcionario, :nome
  json.url funcionario_url(funcionario, format: :json)
end
