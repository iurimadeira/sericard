json.array!(@requisicoes) do |requisicao|
  json.extract! requisicao, :produto_id, :quantidade
  json.url requisicao_url(requisicao, format: :json)
end
