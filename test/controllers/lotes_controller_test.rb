require 'test_helper'

class LotesControllerTest < ActionController::TestCase
  setup do
    @lote = lotes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:lotes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create lote" do
    assert_difference('Lote.count') do
      post :create, lote: { codigo: @lote.codigo, data_entrada: @lote.data_entrada, data_entrega: @lote.data_entrega, data_inicio: @lote.data_inicio, data_termino: @lote.data_termino, funcionario: @lote.funcionario, operadora: @lote.operadora, prioridade: @lote.prioridade, quantidade_cartoes: @lote.quantidade_cartoes, quantidade_kits: @lote.quantidade_kits }
    end

    assert_redirected_to lote_path(assigns(:lote))
  end

  test "should show lote" do
    get :show, id: @lote
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @lote
    assert_response :success
  end

  test "should update lote" do
    patch :update, id: @lote, lote: { codigo: @lote.codigo, data_entrada: @lote.data_entrada, data_entrega: @lote.data_entrega, data_inicio: @lote.data_inicio, data_termino: @lote.data_termino, funcionario: @lote.funcionario, operadora: @lote.operadora, prioridade: @lote.prioridade, quantidade_cartoes: @lote.quantidade_cartoes, quantidade_kits: @lote.quantidade_kits }
    assert_redirected_to lote_path(assigns(:lote))
  end

  test "should destroy lote" do
    assert_difference('Lote.count', -1) do
      delete :destroy, id: @lote
    end

    assert_redirected_to lotes_path
  end
end
