# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Initialize the Rails application.
Sericard::Application.initialize!

# Formato de Data
Date::DATE_FORMATS[:data] = "%d/%m/%y"
