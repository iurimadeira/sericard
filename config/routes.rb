Sericard::Application.routes.draw do
  
  resources :requisicoes

  match 'produtos_entrada_path', 'produtos/entrada/', :controller => 'produtos', :action => 'entrada', :via => 'get'
  match 'produtos_saida_path', 'produtos/saida/', :controller => 'produtos', :action => 'saida', :via => 'get'
  match 'produtos_search_path','search/produtos', :controller => 'produtos', :action => 'search', :via => 'get'

  match 'search/lotes/', :controller => 'lotes', :action => 'search', :via => 'get'
  match 'lotes_finalizados_path', 'lotes/finalizados/', :controller => 'lotes', :action => 'finalizados', :via => 'get'
  match 'relatorio/:id/', :controller => 'lotes', :action => 'relatorio', :via => 'get'
  
  devise_for :funcionarios

  resources :produtos
  resources :lotes, :except => [:show]
  resources :operadoras  
  resources :funcionarios

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root :to => "lotes#index"

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
